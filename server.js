var express = require("express");
var app = express();
var router = express.Router();
var path = __dirname + '/views/'; // this folder should contain your html files.
var publicDir = require('path').join(__dirname,'/public');
app.use(express.static(publicDir));

router.get("/", function(req, res){
  res.sendFile(path + "index.html");
});

router.get("/about", function(req, res){
  res.sendFile(path + "about.html");
});

router.get("/members", function(req, res){
  res.sendFile(path + "members.html");
});

router.get("/credits", function(req, res){
  res.sendFile(path + "credits.html");
});

router.get("/contact", function(req, res){
  res.sendFile(path + "contact.html");
});

app.use("/",router);

app.listen(30000,function(){
  console.log("Live at Port 30000");
});
